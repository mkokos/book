# STX NEXT TEST APP

App created in Python with django

This is test aplication for STX NEXT
## Working app

[Here](https://murmuring-shore-59486.herokuapp.com) You have working app on the serwer.

## Search on main view

On main view You can search by search field:
- title, author, language - You need to write sentence;
- between dates, You need to write two dates in format YYYY-MM-DD devided by ' - ' (space,dash,space);

## API 

API is one endpoint /api which return json :

```sh
{"data": [], "count": 0}
```
in 'data' array you can get objects like this:

```sh
{
  "id": 4,
  "title": "Entertainment Weekly The Ultimate Guide to The Big Bang Theory",
  "author": "The Editors of Entertainment Weekly ",
  "publication_date": "2019-04-26T00:00:00Z",
  "isbn": "1547846534",
  "number_of_pages": 96,
  "cover_url": "http://books.google.com/books/content?id=_RCbDwAAQBAJ&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api",
  "language": "en"
}
```
field 'count' stores size of 'data' array;

You can search and sort API data by url parameters:
- q 
- sort

parameter **q** represents search query, You can search by title, author, language 
Parameter **sort** represent sorting of data, You can sort:
- **ascending** by every of object field using its name 
- **descending** by every field of object using its name prefixed with dash
