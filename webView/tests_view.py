from django.test import TestCase
from django.urls import reverse

from .models import Book


class TestView(TestCase):

    @staticmethod
    def create_book(title="only a test"):
        Book.objects.create(title=title)
        return Book.objects.get(title=title)

    def test_index_list_view(self):
        w = self.create_book()
        url = reverse("index")
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'list.html')
        self.assertContains(resp, w.title)

    def test_search_list_view(self):
        self.create_book()
        url = reverse("book_search")
        url += '?q=only'
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'list.html')
        self.assertContains(resp, "only a test")

    def test_delete_book(self):
        self.create_book()
        url = reverse("book_delete", kwargs={'pk': 1})
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 302)
        self.assertRedirects(resp, '/', status_code=302,
                             target_status_code=200, fetch_redirect_response=True)
        url = reverse("index")
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'list.html')
        self.assertNotContains(resp, "only a test")

    def test_edit_book(self):
        self.create_book()
        url = reverse("book_edit", kwargs={'pk': 1})
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'form.html')

        url = reverse("book_edit", kwargs={'pk': 1})
        resp = self.client.post(url, {'title': 'edited tittle'})
        self.assertEqual(resp.status_code, 302)
        self.assertRedirects(resp, '/', status_code=302,
                             target_status_code=200, fetch_redirect_response=True)
        url = reverse("index")
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'list.html')
        self.assertContains(resp, "edited tittle")

    def test_new_book(self):
        # self.create_book()
        url = reverse("book_new")
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'form.html')

        url = reverse("book_new")
        resp = self.client.post(url, {'title': 'new tittle'})
        self.assertEqual(resp.status_code, 302)
        self.assertRedirects(resp, '/', status_code=302,
                             target_status_code=200, fetch_redirect_response=True)
        url = reverse("index")
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'list.html')
        self.assertContains(resp, "new tittle")

    def test_import_books_from_api(self):
        url = reverse("import_from_api")
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'import.html')
        self.assertEqual(Book.objects.all().count(), 0)
        url = reverse("import_from_api")
        resp = self.client.get(url, {'q': 'The Big Bang Theory', 'order_by': 'newest'})
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'import.html')
        self.assertEqual(Book.objects.all().count(), 10)
        url = reverse("import_from_api")
        resp = self.client.get(url, {'q': 'The Big Bang Theory', 'order_by': 'newest'})
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'import.html')
        self.assertTrue(Book.objects.all().count() < 20)
