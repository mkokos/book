from django.db.models import Q
from django.utils.datetime_safe import datetime

from webView.models import Book


def try_parsing_date(text):
    for fmt in ('%Y-%m-%d', '%Y', '%Y-%m'):
        try:
            return datetime.strptime(text, fmt)
        except ValueError:
            pass
    return None


def search_book(query, sort):
    if query:
        if query.find('-') > 1:
            dates = query.split(sep=' - ', maxsplit=1)
            object_list = Book.objects.filter(
                Q(publication_date__range=(dates[0], dates[1])))
        else:
            object_list = Book.objects.filter(
                Q(title__icontains=query) | Q(author__icontains=query) |
                Q(language__icontains=query))
    else:
        object_list = Book.objects.all()
    if sort:
        object_list = object_list.order_by(sort)
    return object_list
