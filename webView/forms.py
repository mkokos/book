import re

from django import forms

from .models import Book


def check(isbn):
    isbn = isbn.replace("-", "").replace(" ", "").upper();
    match = re.search(r'^(\d{9})(\d|X)$', isbn)
    if not match:
        return False

    digits = match.group(1)
    check_digit = 10 if match.group(2) == 'X' else int(match.group(2))

    result = sum((i + 1) * int(digit) for i, digit in enumerate(digits))
    return (result % 11) == check_digit


class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = ('title', 'author',
                  'language', 'number_of_pages',
                  'publication_date', 'isbn',
                  'cover_url',)

    def clean(self):
        super(BookForm, self).clean()

        # title = self.cleaned_data.get("title")
        # author = self.cleaned_data.get("author")
        isbn = self.cleaned_data.get("isbn")
        if isbn:
            if not check(isbn):
                self._errors['isbn'] = self.error_class(['To nie jest poprawny ISBN-10'])
