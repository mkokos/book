import json

from django.test import TestCase
from django.urls import reverse

from .models import Book


class TestModel(TestCase):
    def setUp(self):
        Book.objects.create(title='dom',
                            author='ja',
                            publication_date='1993-12-21',
                            number_of_pages=12,
                            isbn='3499628929',
                            cover_url=None,
                            language='pl')
        Book.objects.create(title='home',
                            author='me',
                            publication_date='1996-04-11',
                            number_of_pages=45,
                            isbn='0762458372',
                            cover_url=None,
                            language='en')
        Book.objects.create(title='domena',
                            author='ktos',
                            publication_date='2001-04-11',
                            number_of_pages=234,
                            isbn='0762458372',
                            cover_url=None,
                            language='pl')

    def test_api_without_params(self):
        url = reverse("search_api")
        resp = self.client.get(url, format='json')
        self.assertEqual(resp.status_code, 200)
        api_response = json.loads(resp.content)
        self.assertEqual(len(api_response['data']), 3)

    def test_api_with_q(self):
        url = reverse("search_api")
        resp = self.client.get(url, {'q': 'domena'}, format='json')
        self.assertEqual(resp.status_code, 200)
        api_response = json.loads(resp.content)
        self.assertEqual(len(api_response['data']), 1)

    def test_api_with_sortq(self):
        url = reverse("search_api")
        resp = self.client.get(url, {'sort': 'title'}, format='json')
        self.assertEqual(resp.status_code, 200)
        api_response = json.loads(resp.content)
        self.assertEqual(len(api_response['data']), 3)
        self.assertEqual(api_response['data'][0]['title'], 'dom')
        self.assertEqual(api_response['data'][1]['title'], 'domena')
        self.assertEqual(api_response['data'][2]['title'], 'home')

    def test_api_with_params(self):
        url = reverse("search_api")
        resp = self.client.get(url, {'q': 'dom', 'sort': "title"}, format='json')
        self.assertEqual(resp.status_code, 200)
        api_response = json.loads(resp.content)
        self.assertEqual(len(api_response['data']), 2)
        self.assertEqual(api_response['data'][0]['title'], 'dom')
        self.assertEqual(api_response['data'][1]['title'], 'domena')
