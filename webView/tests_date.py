from django.test import TestCase
from django.utils.datetime_safe import datetime

from .services import try_parsing_date


class TestDate(TestCase):

    def test_parsing_date_year(self):
        self.assertEqual(try_parsing_date('1202'), datetime.strptime('1202-01-01 00:00:00', '%Y-%m-%d %H:%M:%S'))

    def test_parsing_date_year_month(self):
        self.assertEqual(try_parsing_date('1202-12'), datetime.strptime('1202-12-01 00:00:00', '%Y-%m-%d %H:%M:%S'))

    def test_parsing_date_year_month_day(self):
        self.assertEqual(try_parsing_date('1202-12-31'), datetime.strptime('1202-12-31 00:00:00', '%Y-%m-%d %H:%M:%S'))

    def test_parsing_date_wrong_format(self):
        self.assertNotEqual(try_parsing_date('1202/12/12'),
                            datetime.strptime('1202-12-12 00:00:00', '%Y-%m-%d %H:%M:%S'))
        self.assertEqual(try_parsing_date('1202/12/12'), None)
