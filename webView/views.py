import requests
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404

from .forms import BookForm
from .models import Book
from .services import try_parsing_date, search_book


def index(request):
    books = Book.objects.all()
    return render(request, "list.html", {"books": books})


def book_edit(request, pk):
    book = get_object_or_404(Book, pk=pk)
    if request.method == "POST":
        form = BookForm(request.POST, instance=book)
        if form.is_valid():
            book = form.save(commit=False)
            book.save()
            return redirect('index', )
    else:
        form = BookForm(instance=book)
    return render(request, 'form.html', {'form': form})


def book_delete(request, pk):
    book = get_object_or_404(Book, pk=pk)
    book.delete()
    return redirect('index', )


def book_new(request):
    if request.method == "POST":
        form = BookForm(request.POST)
        if form.is_valid():
            book = form.save(commit=False)
            book.save()
            return redirect('index', )
    else:
        form = BookForm()
    return render(request, 'form.html', {'form': form})


def search(request):
    query = request.GET.get('q')
    sort = request.GET.get("sort")
    object_list = search_book(query, sort)
    return render(request, 'list.html', {'books': object_list})


def import_book_from_api(request):
    query = request.GET.get('q')
    if query:
        response = requests.get(
            'https://www.googleapis.com/books/v1/volumes?q=' + query).json()
        for item in response['items']:
            vol_info = item['volumeInfo']
            book = Book()
            book.title = vol_info["title"]
            str_author = str('')
            if "authors" in vol_info:
                for author in vol_info["authors"]:
                    str_author += str(author) + " "
                book.author = str_author
            if "industryIdentifiers" in vol_info:
                for isbn in vol_info["industryIdentifiers"]:
                    if isbn['type'] == "ISBN_10":
                        book.isbn = isbn["identifier"]
            if "imageLinks" in vol_info:
                book.cover_url = vol_info["imageLinks"]["smallThumbnail"]
            if "pageCount" in vol_info:
                book.number_of_pages = vol_info["pageCount"]
            if "publishedDate" in vol_info:
                book.publication_date = try_parsing_date(vol_info["publishedDate"])
            if "language" in vol_info:
                book.language = vol_info["language"]
            if any(x.is_same(book) for x in Book.objects.all()):
                continue
            book.save()
    else:
        response = {}
    return render(request, 'import.html', {'response': response})


def search_api(request):
    query = request.GET.get('q')
    sort = request.GET.get("sort")
    object_list = search_book(query, sort)
    return JsonResponse({"data": list(object_list.values()), "count": object_list.count()})
