from django.test import TestCase

from .forms import BookForm, check
from .models import Book


class TestForm(TestCase):

    def test_valid_form_only_title(self):
        w = Book.objects.create(title='test', author=None, language=None, number_of_pages=None, publication_date=None,
                                isbn=None, cover_url=None)
        data = {'title': w.title, 'author': w.author, 'language': w.language, 'number_of_pages': w.number_of_pages,
                'publication_date': w.publication_date, 'isbn': w.isbn, 'cover_url': w.cover_url, }
        form = BookForm(data=data)
        self.assertTrue(form.is_valid())

    def test_invalid_form_no_title(self):
        data = {'title': None, 'author': None, 'language': None, 'number_of_pages': None,
                'publication_date': None, 'isbn': None, 'cover_url': None, }
        form = BookForm(data=data)
        self.assertFalse(form.is_valid())

    def test_isbn_check(self):
        list_valid_isbn = {'3656226377', '1547846542', '8506021812', '158503620X'}
        list_invalid_isbn = {'365622237733', '1544442', '850345812', '12220X'}
        for isbn in list_valid_isbn:
            self.assertTrue(check(isbn))
        for isbn in list_invalid_isbn:
            self.assertFalse(check(isbn))

    def test_valid_form_isbn(self):
        list_isbn = {'3656226377', '1547846542', '8506021812', '158503620X'}
        for isbn in list_isbn:
            data = {'title': "TITLE", 'author': None, 'language': None, 'number_of_pages': None,
                    'publication_date': None, 'isbn': None, 'cover_url': None, }
            form = BookForm(data=data)
            self.assertTrue(form.is_valid())

    def test_invalid_form_isbn(self):
        list_isbn = {'36562223731', '1544442', '8503495812', '12220X'}
        for isbn in list_isbn:
            data = {'title': "TITLE", 'author': None, 'language': None, 'number_of_pages': None,
                    'publication_date': None, 'isbn': isbn, 'cover_url': None, }
            form = BookForm(data=data)
            self.assertFalse(form.is_valid())
            self.assertEqual(form.errors['isbn'][0], 'To nie jest poprawny ISBN-10')
