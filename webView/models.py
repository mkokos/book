from django.db import models


# Create your models here.


class Book(models.Model):
    title = models.CharField("Tytuł", max_length=255)
    author = models.CharField("Autor", max_length=255, blank=True, null=True)
    publication_date = models.DateField("Data publikacji (RRRR-MM-DD)", null=True, blank=True)
    isbn = models.CharField("ISBN-10", max_length=11, null=True, blank=True)
    number_of_pages = models.PositiveIntegerField("Ilość stron", null=True, blank=True)
    cover_url = models.URLField('Link do okładki', max_length=255, null=True, blank=True)
    language = models.CharField('Język', max_length=255, null=True, blank=True)

    class Meta:
        ordering = ['title']

    def is_same(self, other):
        return self.title == other.title \
               and self.author == other.author \
               and self.isbn == other.isbn \
               and self.number_of_pages == other.number_of_pages \
               and self.cover_url == other.cover_url \
               and self.language == other.language \
               and self.publication_date == other.publication_date.date()
