from datetime import date

from django.db.models import Q
from django.test import TestCase
from django.utils.datetime_safe import datetime

from .models import Book
from .services import search_book


class TestModel(TestCase):
    def setUp(self):
        Book.objects.create(title='dom',
                            author='ja',
                            publication_date='1993-12-21',
                            number_of_pages=12,
                            isbn='3499628929',
                            cover_url=None,
                            language='pl')
        Book.objects.create(title='home',
                            author='me',
                            publication_date='1996-04-11',
                            number_of_pages=45,
                            isbn='0762458372',
                            cover_url=None,
                            language='en')
        Book.objects.create(title='domena',
                            author='ktos',
                            publication_date='2001-04-11',
                            number_of_pages=234,
                            isbn='0762458372',
                            cover_url=None,
                            language='pl')

    def test_books_are_same_not_equal(self):
        book1 = Book.objects.get(title__contains='domena')
        book2 = Book.objects.create(title='domena',
                                    author='ktos',
                                    publication_date=datetime(2001, 4, 11),
                                    number_of_pages=234,
                                    isbn='0762458372',
                                    cover_url=None,
                                    language='pl')
        self.assertTrue(book1.is_same(book2))
        self.assertNotEqual(book1, book2)

    def test_search_service_title(self):
        self.assertQuerysetEqual(search_book('home', None), Book.objects.filter(title='home'))

    def test_search_service_date(self):
        self.assertEqual(search_book('1993-01-01 - 2000-12-12', None).count(), 2)
        self.assertQuerysetEqual(search_book('1993-01-01 - 2000-12-12', None), Book.objects.filter(
            Q(publication_date__range=('1993-01-01', '2000-12-12'))))

    def test_search_service_part(self):
        self.assertEqual(search_book('me', None).count(), 2)
        self.assertQuerysetEqual(search_book('me', None), Book.objects.filter(Q(title__icontains='me') |
                                                                              Q(author__icontains='me') |
                                                                              Q(language__icontains='me')))

    def test_search_service_with_sort(self):
        self.assertQuerysetEqual(search_book('dom', 'author'),
                                 Book.objects.filter(Q(title__icontains='dom') |
                                                     Q(author__icontains='dom') |
                                                     Q(language__icontains='dom')).order_by('author'))

    def test_search_service_sort(self):
        self.assertQuerysetEqual(search_book(None, 'title'), Book.objects.order_by('title'))

    def test_search_service_count(self):
        self.assertEqual(3, Book.objects.all().count())
