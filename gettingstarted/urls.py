from django.urls import path, include

from django.contrib import admin
import webView.views

admin.autodiscover()

# To add a new path, first import the gettingstarted:
# import blog
#
# Then add the new path:
# path('blog/', blog.urls, name="blog")
#
# Learn more here: https://docs.djangoproject.com/en/2.1/topics/http/urls/

urlpatterns = [
    path("", webView.views.index, name="index"),
    path('book/new', webView.views.book_new, name='book_new'),
    path('book/search', webView.views.search, name='book_search'),
    path('api', webView.views.search_api, name='search_api'),
    path('import', webView.views.import_book_from_api, name='import_from_api'),
    path('book/<int:pk>/edit', webView.views.book_edit, name='book_edit'),
    path('book/<int:pk>/delete', webView.views.book_delete, name='book_delete'),
    # path("admin/", admin.site.urls),
]
